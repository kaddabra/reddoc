import React, {Component} from 'react';
import {Text,View,StyleSheet} from 'react-native';
import{Container,Content} from 'native-base';
import {connect} from 'react-redux';

export default class Verification extends Component{  
    
    render(){
        return(
            <Container>            
                <Content contentContainerStyle = {styles.styleTotal}>
                    <View style = {styles.view}>
                        <Text style = {styles.Text2}>
                        in construction
                        </Text>
                    </View>
                </Content>    
            </Container>
        )
    }
}
const styles = StyleSheet.create(
    {
        styleTotal:{
            flex: 1,
            backgroundColor: '#D6D6D6',
            justifyContent: 'center',
            alignItems: 'center'
        },
        Text2:{
            fontWeight:'bold',
            color: '#01071A',
            fontSize: 13,  
        },
        view:{
            margin: '10%',
            backgroundColor: 'transparent'
        }
    }
)