import React, {Component} from 'react';
import {Text,View,StyleSheet,TouchableOpacity,Modal,TextInput} from 'react-native';
import firebase from 'react-native-firebase';
import{Container,Content} from 'native-base';
import {connect} from 'react-redux';

class Profile extends Component{  
    
    constructor(){
        super();
        this.state = {
            display: false,
            name: '',
            lastname: '',
            telephone: '',
            email: ''
        }
    }

    componentDidMount(){
        this.db=firebase.firestore();        
    }

    triggerModal(visible) {
        this.setState({display: visible});
    }

    setName = (name) => {
        this.setState({
            name: name
        })
    }

    setLastname = (lastname) => {
        this.setState({
            lastname: lastname
        })
    }

    setTelephone = (telephone) => {
        this.setState({
            telephone: telephone
        })
    }

    setEmail = (email) => {
        this.setState({
            email: email
        })
    }

    _logOut = async () => {
        firebase.auth().signOut()
           .then(() => this.props.navigation.navigate('Loading'))
    }   

    render(){ 
        //console.log(this.props.event)
        return(
            <Container>            
                <Content contentContainerStyle = {styles.styleTotal}>
                    <View style = {styles.view}>     
                        <Text style = {styles.Text}>Name: </Text>
                        <Text style = {styles.Text2}>{this.props.event.Nombre}</Text> 
                        <Text style = {styles.Text}>Lastname: </Text>   
                        <Text style = {styles.Text2}>{this.props.event.Apellido}</Text> 
                        <Text style = {styles.Text}>Telephone</Text>
                        <Text style = {styles.Text2}>{this.props.event.Telefono}</Text>  
                        <Text style = {styles.Text}>Email</Text> 
                        <Text style = {styles.Text2}>{this.props.event.Email}</Text> 
                        <View style={{flexDirection: 'row'}}>  
                            <TouchableOpacity style = {styles.bottonLogin}
                                    onPress = {() => this.triggerModal(true)}>
                                <Text style = {styles.Text3}>Edit</Text>  
                            </TouchableOpacity>                    
                            <TouchableOpacity style = {styles.bottonLogin}
                                onPress = {this._logOut}>
                                <Text style = {styles.Text3}>LogOut</Text>  
                            </TouchableOpacity>   
                        </View> 
                    </View>
                    <Modal visible = {this.state.display} animationType = "slide" transparent = {false}>
                        <View style={{marginTop:'20%', alignItems:'center',flex:1,backgroundColor:'white'}}>
                        <Text>Por favor llenar todos los datos (BUG en reparacion)</Text>
                            <Text>Name:</Text>
                            <TextInput 
                                style ={styles.textinputStyle}
                                autoCapitalize = 'none'
                                autoCorrect = {false}
                                maxLength = {22} 
                                placeholder = {this.props.event.Nombre}
                                placeholderTextColor = "black"
                                value = {this.state.Name}
                                onChangeText = {(text) => this.setName(text)}
                            />     
                            <Text>Lastname:</Text>                                                      
                            <TextInput 
                                style ={styles.textinputStyle}
                                textContentType = 'none'
                                autoCapitalize = 'none'
                                autoCorrect = {false}
                                maxLength = {22} 
                                placeholder = {this.props.event.Apellido}
                                placeholderTextColor = "black"
                                value = {this.state.lastname}
                                onChangeText = {(text) => this.setLastname(text)}
                            /> 
                            <Text>Telephone:</Text>                                                      
                            <TextInput 
                                style ={styles.textinputStyle}
                                textContentType = 'none'
                                autoCapitalize = 'none'
                                autoCorrect = {false}
                                maxLength = {22} 
                                placeholder = {this.props.event.Telefono}
                                placeholderTextColor = "black"
                                value = {this.state.telephone}
                                onChangeText = {(text) => this.setTelephone(text)}
                            /> 
                            <Text>Email:</Text>                                                      
                            <TextInput 
                                style ={styles.textinputStyle}
                                textContentType = 'none'
                                autoCapitalize = 'none'
                                autoCorrect = {false}
                                maxLength = {30} 
                                placeholder = {this.props.event.Email}
                                placeholderTextColor = "black"
                                value = {this.state.email}
                                Text = {this.props.event.Email}
                                onChangeText = {(text) => this.setEmail(text)}
                            />                         
                                                                            
                            <View style={{alignItems:'center', flexDirection: 'row'}}>
                                <TouchableOpacity style = {styles.cambio}
                                    onPress = {()=>{
                                        this.db.collection('Users').doc(this.props.user.uid).update({
                                            Nombre: this.state.name,
                                            Apellido: this.state.lastname,
                                            Email: this.state.email,
                                            Telefono: this.state.telephone
                                        })
                                        this.setState({display:false})
                                        }}>
                                    <Text style = {styles.TextB}>AGREGAR</Text>                      
                                </TouchableOpacity>
                                <TouchableOpacity style = {styles.cambio}
                                    onPress = {()=>{this.setState({display:false})}}>
                                    <Text style = {styles.TextB}>CANCELAR</Text>                      
                                </TouchableOpacity>
                            </View>   
                        </View>
                    </Modal>
                </Content>    
            </Container>
        )
    }
}
export default connect((state) => {return {event: state.event};})(Profile)

const styles = StyleSheet.create(
    {
        styleTotal:{
            flex: 1,
            backgroundColor: '#D6D6D6',
            justifyContent: 'center',
            alignItems: 'center'
        },
        bottonLogin: {
            backgroundColor: 'black',
            padding: 12,
            margin: 8,
            marginTop:'20%',
            width:'45%',
            height:'40%',
            borderRadius: 12,
            alignItems: 'center'
        }, 
        Text:{
            fontWeight:'bold',
            color: '#01071A',
            fontSize: 18,  
        },
        TextB:{
            fontWeight:'bold',
            color: 'white',
            fontSize: 13,  
        },
        Text2:{
            fontWeight:'bold',
            color: '#B09805',
            fontSize: 18,  
        },
        Text3:{
            fontWeight:'bold',
            color: 'white',
            fontSize: 18,  
        },
        view:{
            margin: '10%',
            backgroundColor: 'transparent'
        },
        textinputStyle:{
            margin: 8,
            width:'80%', 
            height: 40,
            borderColor: '#B09805',
            borderRadius: 15,
            borderWidth: 1,
            color: 'black',
            backgroundColor: 'white',
        },
        cambio: {
            backgroundColor: '#01071A',
            padding: 13,
            margin: 3,
            width:'40%',
            height: 35,
            borderRadius: 15,
            alignItems: 'center'
          },
    }
)