import React, {Component} from 'react';
import {Text,View,StyleSheet,TouchableOpacity,TextInput} from 'react-native';
import{Container,Content} from 'native-base';
import {connect} from 'react-redux';
 

class News extends Component{  

    constructor() {
        super();
        this.state = {            
            news: ''
        }
    }

    setNews = (news) => {
        this.setState({
            news: news
        })
    }

    render(){
        return(
            <Container>            
                <Content contentContainerStyle = {styles.styleTotal}>
                    <View style = {styles.view}>                                                   
                        <TextInput 
                            style ={styles.textinputStyle}
                            textContentType = 'none'
                            autoCapitalize = 'none'
                            autoCorrect = {false}
                            maxLength = {100} 
                            placeholder = 'News'
                            placeholderTextColor = "black"
                            value = {this.state.news}
                            Text = 'News'
                            onChangeText = {(text) => this.setNews(text)}
                        />
                        <TouchableOpacity style = {styles.cambio}
                            onPress = {()=>{}}>
                            <Text style = {styles.TextB}>Add News</Text>                      
                         </TouchableOpacity>
                    </View>
                </Content>    
            </Container>
        )
    }
}
export default connect((state) => {return {news: state.news};})(News)
 
  
const styles = StyleSheet.create(
    {
        styleTotal:{
            flex: 1,
            backgroundColor: '#D6D6D6',
            alignItems: 'center'
        },
        Text2:{
            fontWeight:'bold',
            color: '#01071A',
            fontSize: 13,  
        },
        TextB:{
            fontWeight:'bold',
            color: 'white',
            fontSize: 15,  
        },
        view:{
            backgroundColor: 'transparent',
            flexDirection: 'row'
        },
        cambio: {
            backgroundColor: '#01071A',
            padding: 10,
            margin: 4,
            width:'30%',
            height: '60%',
            borderRadius: 15,
            alignItems: 'center'
          },
          textinputStyle:{
            margin: 8,
            width:'60%', 
            height: '60%',
            borderColor: '#B09805',
            borderRadius: 15,
            borderWidth: 1,
            color: 'black',
            backgroundColor: 'white',
        },  
    }
)