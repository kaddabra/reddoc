import React, {Component} from 'react';
import firebase from 'react-native-firebase';
import News from './screens/News';
import {connect} from 'react-redux';
import {addNews} from '../../actions/news';

class ProfileS extends Component{

    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.db = firebase.firestore();
    }


    render(){
        return(
            <News/>
        );
    }

}
export default connect ((state) => {return {user:state.user};},{addNews})(ProfileS)