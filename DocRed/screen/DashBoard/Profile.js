import React, {Component} from 'react';
import firebase from 'react-native-firebase';
import Profile from './screens/Profile';
import {addEvent} from '../../actions/events'; 
import {connect} from 'react-redux';

class ProfileS extends Component{

    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.db = firebase.firestore();
        this.readPayments()
    }

    readPayments = async() => {
        let ref = await this.db.collection('Users').doc(this.props.user.uid);
        ref.onSnapshot((querySnapshot) => {            
                this.props.addEvent(querySnapshot.data())
            });
        }   

    render(){
        return(
            <Profile/>
        );
    }

}
export default connect ((state) => {return {user:state.user};},{addEvent})(ProfileS)