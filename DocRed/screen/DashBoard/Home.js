import React, { Component} from 'react';
import { View, Image, TouchableOpacity,SafeAreaView, Text, ScrollView} from 'react-native';
import {Icon} from 'native-base';
import {   
  createAppContainer,
  createSwitchNavigator  
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator,  DrawerItems } from 'react-navigation-drawer';
import Ionicons from 'react-native-vector-icons/Ionicons';

 
import ProfileS from './Profile'
import News from './News';
import Friends from './screens/Friends';
import Verfication from './screens/Verification';
import AboutUs from './screens/AboutUs';

class NavigationDrawerStructure extends Component {
 
    toggleDrawer = () => {
      this.props.navigationProps.toggleDrawer();
    }

  render() {
    
    return (           
      <View style={{ flexDirection: 'row', margin: 15}}>
        <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
          <Icon 
          name = 'menu' 
          size = {40}
          style = {{color: 'white'}}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const DrawerMenu = (props) => (
  <SafeAreaView style={{ flex: 1 }}>
      <View style ={{alignItems:'center'}}>                        
          <View >           
            <Text style = {{color: 'white',fontSize: 32,marginBottom:6}}>DocRed App</Text>
            <Text style = {{color: 'white',fontSize: 15,marginBottom:6}}>Welcome</Text>
          </View>    
      </View>
      <ScrollView>
          <DrawerItems {...props}>
          </DrawerItems>
      </ScrollView>
      <View style ={{alignItems:'center'}}>
          <Text style = {{color: 'white',fontSize: 12,}}>Versión 1.0.0</Text>
          <Text style = {{color: 'white',fontSize: 12,}}>2019</Text>
      </View> 
  </SafeAreaView>
)

const NewsScreen = createStackNavigator({
  First: {
    screen: News,
    navigationOptions: ({ navigation }) => ({
      title: 'News',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#B09805',
      },
      headerTintColor: '#fff',
    }),
  },
});
const ProfileScreen = createStackNavigator({
  Secont: {
    screen: ProfileS,
    navigationOptions: ({ navigation }) => ({      
      title: 'Profile',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#B09805' , 
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }),
  },
}); 
const FriendsScreen = createStackNavigator({
  Third: {
    screen: Friends,
    navigationOptions: ({ navigation }) => ({
      title: 'Friends',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#B09805',
      },
      headerTintColor: '#fff',
    }),
  },
});
const VerificationScreen = createStackNavigator({
  Third: {
    screen: Verfication,
    navigationOptions: ({ navigation }) => ({
      title: 'Verification',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#B09805',
      },
      headerTintColor: '#fff',
    }),
  },
});
const AboutUsScreen = createStackNavigator({
  Third: {
    screen: AboutUs,
    navigationOptions: ({ navigation }) => ({
      title: 'About Us',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#B09805',
      },
      headerTintColor: '#fff',
    }),
  },
});
 
const DrawerNavigatorMenu = createDrawerNavigator(
  {
    News: {
      screen: NewsScreen,
      navigationOptions: {
        drawerLabel: 'News',
        drawerIcon: () => <Ionicons name = 'md-book' size={24} color = 'white' /> 
      },
    },
    ProfileS: {
      screen: ProfileScreen,
      navigationOptions: {
        drawerLabel: 'Profile', 
        drawerIcon: () => <Ionicons name = 'md-contact' size={24} color = 'white' />              
      },
    },    
    Friends: {
      screen: FriendsScreen,
      navigationOptions: {
        drawerLabel: 'Friends',
        drawerIcon: () => <Ionicons name = 'md-contacts' size={24} color = 'white' />
      },
    },
    Bonus: {
      screen: VerificationScreen,
      navigationOptions: {
        drawerLabel: 'Verification',
        drawerIcon: () => <Ionicons name = 'md-checkmark-circle-outline' size={24} color = 'white'/>
      },
    },
    AboutUs: {
      screen: AboutUsScreen,
      navigationOptions: {
        drawerLabel: 'About Us',
        drawerIcon: () => <Ionicons name = 'md-alert' size={24} color = 'white' />
      },
    },
  },
  {
    initialRouteName: 'News',
    drawerBackgroundColor: '#B09805',
    drawerPosition: 'left',
    drawerType : 'slide',
    drawerWidth: 220,
    contentOptions: {
      activeTintColor: '#817004',
      inactiveTintColor :'white',
      activeBackgroundColor :'#96A1CA'
    },
    contentComponent: DrawerMenu,
  }
);
 
const totalScreens = createAppContainer(DrawerNavigatorMenu)

const AppAlerts = createSwitchNavigator(
  {   
      Home: totalScreens    
  },
  {
      initialRouteName: 'Home'
  }
)

export default createAppContainer(AppAlerts);
