import React,{Component} from 'react';
import {createSwitchNavigator, createAppContainer} from "react-navigation";

import Loading from './Loading'
import LoginScreen from './screens/loginScreen';
import RegisterScreen from './screens/RegisterScreen';
import ForgotPassword from './ForgotPass';
import logDates from '../DashBoard/logDates';
import { Provider } from 'react-redux';
import store from '../../store';


const AppNavigator = createSwitchNavigator(
    {       
        Loading: Loading,
        Login: LoginScreen,  
        Register: RegisterScreen,  
        Home: logDates,
        Fpw: ForgotPassword,     
    },
    {
        initialRouteName: 'Loading'
    }
)
const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component{      
    render(){
        return(
            <Provider store = {store}>
                <AppContainer/>
            </Provider>    
        );
    }
};