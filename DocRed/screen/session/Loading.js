import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    ImageBackground,
    Text,      
} from 'react-native';
import {connect} from 'react-redux';
import {login} from '../../actions/user';
import firebase from 'react-native-firebase';

 class Loading extends Component{
    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            this.props.login(user);
            if(user){
                this.props.navigation.navigate('Home');                
            }
            else{
                this.props.navigation.navigate('Login');        
            }                      
        })
    }
   
    render(){
        return(
            <View style = {{flex: 1}}>
                <ImageBackground
                source = {require('../../assets/home.jpg')}
                style = {{flex:1}}> 
                    <Text>CARGANDO ...</Text>                    
                </ImageBackground> 
            </View>
        );
    }
}

export default connect ((state) => {
    return {user:state.user};},{
    login
})(Loading)

const styles = StyleSheet.create(
    {
        contentLogo:{
            marginTop: '50%', 
            alignItems: 'center',          
        },
        Text:{
            fontWeight:'bold',
            color: 'white',
            fontSize: 18,  
        }
    }
)