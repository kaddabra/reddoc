import React, {Component}from 'react';
import {
    View,
    Text,
    StyleSheet,
    ImageBackground,
    Image,
    TouchableHighlight,
    TouchableOpacity,
    TextInput
} from 'react-native'

export default class login extends Component{ 
    constructor (props){
        super(props);
        this.state = {
            email: '',
            password: ''            
        }
    }
    setEmail = (email) => {
        this.setState({
            email: email
        })
    }

    setPassword = (password) => {
        this.setState({
            password: password
        })
    }
    render(){
        let props = this.props;
        return(
            <View style = {{flex:1}}>
                <ImageBackground
                source = {require('../../../assets/home.jpg')}
                style = {{flex:1}}>                    
                    <View style = {styles.contentAll}>
                        <Text style = {styles.TextT}>RedDoc:</Text>
                        <Text style = {styles.TextT}>Doctors Network</Text>
                        <TextInput style ={styles.textinputStyle}
                            textContentType = 'emailAddress'
                            keyboardType = 'email-address'
                            autoCapitalize = 'none'
                            autoCorrect = {false}
                            underlineColorAndroid = "transparent"
                            placeholder = "Email"
                            placeholderTextColor = "black"
                            value = {this.state.email}
                            onChangeText = {(text) => this.setEmail(text)}
                        />
                        <TextInput style = {styles.textinputStyle}
                            secureTextEntry = {true}
                            textContentType = 'password' 
                            underlineColorAndroid = "transparent"
                            placeholder = "Password"
                            placeholderTextColor = "black"                            
                            autoCapitalize = "none"
                            value = {this.state.password}
                            onChangeText = {(text) => this.setPassword(text)}
                        />
                        <TouchableOpacity style = {styles.bottonLogin}
                            onPress = {() => {props.loginUser({email:this.state.email, password:this.state.password})}}>
                            <Text style = {styles.Text}>Login</Text>  
                        </TouchableOpacity>
                        <TouchableHighlight onPress={() => props.sinContraseña()}>
                            <Text style = {styles.Text4}>Forgot Password</Text>
                        </TouchableHighlight>
                        <View style = {styles.contentReg}>
                            <Text style = {styles.Text2}>don't have an account:  </Text>
                            <TouchableHighlight onPress={() => props.resgistropantalla()}>
                                <Text style = {styles.Text3}>Sign In</Text>
                            </TouchableHighlight>
                        </View>                       
                    </View>            
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create(
    {
        contentLogo:{
            marginTop: '13%', 
            alignItems: 'center',          
        },
        TextT:{
            fontWeight:'bold',
            color: '#B09805',
            fontSize: 28,  
        },
        Text:{
            fontWeight:'bold',
            color: 'white',
            fontSize: 18,  
        },
        Text2:{
            color: 'black',
            fontSize: 14,
        },
        Text3:{
            color: '#EE4141',
            fontSize: 14,            
        },
        Text4:{
            color: 'black',
            fontSize: 14,
            textDecorationLine: 'underline'
        },
        contentAll:{
            marginTop: '30%',
            alignItems: 'center',
            height: '70%',
        },
        textinputStyle:{
            margin: 8,
            width:'80%', 
            height: 40,
            borderColor: '#B09805',
            borderRadius: 15,
            borderWidth: 1,
            color: 'black',
        },
        bottonLogin: {
            backgroundColor: '#B09805',
            padding: 10,
            margin: 8,
            width:'80%',
            height: 40,
            borderRadius: 15,
            alignItems: 'center'
        }, 
        contentReg:{
            alignItems: 'center',
            flexDirection: 'row',
            marginTop: '10%'
        }
    }
)