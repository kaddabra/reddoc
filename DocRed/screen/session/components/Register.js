import React,{Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    ImageBackground,
    TouchableHighlight,
    TouchableOpacity,    
    TextInput,    
} from 'react-native'

export default class Register extends Component { 
    
        constructor(props){
            super(props);
            this.state = {
                email: '',
                password: '', 
                name: '',
                lastname: '',
                cel: '',
                cpassword: ''
            }
        }

        
        setEmail = (email) => {
            this.setState({
                email: email
            })
        }

        setName = (name) => {
            this.setState({
                name: name
            })
        }

        setLastname = (lastname) => {
            this.setState({
                lastname: lastname
            })
        }

        setCel = (cel) => {
            this.setState({
                cel: cel
            })
        }

        setPassword = (password) => {
            this.setState({
                password: password
            })
        }

        setCpassword = (cpassword) => {
            this.setState({
                cpassword: cpassword
            })
        }   

    render(){    
        let props = this.props;
        return(
            <View style = {{flex:1}}>
                <ImageBackground
                source = {require('../../../assets/home.jpg')}
                style = {{flex:1}}> 
                    <View style = {styles.contentAll}>
                    <Text style = {styles.Text}>Registro</Text>
                    
                    <TextInput style ={styles.textinputStyle}
                        underlineColorAndroid = "transparent"
                        autoCorrect = {false}
                        placeholder = "Nombre"
                        placeholderTextColor = "black"
                        autoCapitalize = "none"    
                        value = {this.state.name}
                        onChangeText = {(text) => this.setName(text)}                   
                    />
                    <TextInput style ={styles.textinputStyle}
                        underlineColorAndroid = "transparent"
                        autoCorrect = {false}
                        placeholder = "Apellidos"
                        placeholderTextColor = "black"
                        autoCapitalize = "none"
                        value = {this.state.lastname}
                        onChangeText = {(text) => this.setLastname(text)}                                               
                    />
                    <TextInput style ={styles.textinputStyle}
                        textContentType = 'telephoneNumber'
                        keyboardType = 'number-pad'
                        underlineColorAndroid = "transparent"
                        autoCorrect = {false}
                        placeholder = " Número celular"
                        placeholderTextColor = "black"
                        autoCapitalize = "none"
                        maxLength = {10}   
                        value = {this.state.cel} 
                        onChangeText = {(text) => this.setCel(text)}                  
                    />
                    <TextInput style ={styles.textinputStyle}
                        textContentType = 'emailAddress'
                        keyboardType = 'email-address'
                        underlineColorAndroid = "transparent"
                        autoCorrect = {false}
                        placeholder = "Email"
                        placeholderTextColor = "black"
                        autoCapitalize = "none"
                        value = {this.state.email}
                        onChangeText = {(text) => this.setEmail(text)} 
                    />
                    <TextInput style = {styles.textinputStyle}
                        secureTextEntry = {true}
                        textContentType = 'password' 
                        underlineColorAndroid = "transparent"
                        placeholder = "Contraseña"
                        placeholderTextColor = "black"                            
                        autoCapitalize = "none"
                        value = {this.state.password}
                        onChangeText = {(text) => this.setPassword(text)} 
                    />
                        <TextInput style = {styles.textinputStyle}
                        secureTextEntry = {true}
                        textContentType = 'password' 
                        underlineColorAndroid = "transparent"
                        placeholder = "Confirmar contraseña"
                        placeholderTextColor = "black"                            
                        autoCapitalize = "none"
                        value = {this.state.cpassword}
                        onChangeText = {(text) => this.setCpassword(text)} 
                    />
                    <TouchableOpacity style = {styles.bottonLogin}
                    onPress={() => {props.createUser({
                        email: this.state.email,
                        password: this.state.password,
                        name: this.state.name,
                        lastname: this.state.lastname,
                        cel: this.state.cel,
                        cpassword: this.state.cpassword
                    })}}>
                        <Text style = {styles.Text}>Registrar</Text>  
                    </TouchableOpacity>
                    <View style = {styles.contentReg}>
                        <Text style = {styles.Text2}>Ya tengo una cuenta:  </Text>
                        <TouchableHighlight onPress={() => props.pantallaLogin()}>
                            <Text style = {styles.Text3}>Login</Text>
                        </TouchableHighlight>
                    </View>                       
                </View>            
            </ImageBackground>
        </View>
        );   
    } 
}

const styles = StyleSheet.create(
    {
        Text:{
            fontWeight:'bold',
            color: 'black',
            fontSize: 18,  
        },
        Text2:{
            color: 'black',
            fontSize: 12,
        },
        Text3:{
            color: '#B09805',
            fontSize: 12,
        },
        contentAll:{
            alignItems: 'center',
            marginTop: '10%'
        },
        textinputStyle:{
            margin: 8,
            width:'80%', 
            height: 40,
            borderColor: '#B09805',
            borderRadius: 15,
            borderWidth: 1,
            color: 'black',
        },
        bottonLogin: {
            backgroundColor: '#B09805',
            padding: 10,
            margin: 8,
            width:'80%',
            height: 40,
            borderRadius: 15,
            alignItems: 'center'
        }, 
        contentReg:{
            alignItems: 'center',
            flexDirection: 'row',
            marginTop: '10%'
        }
    }
)