import React, {Component} from 'react';
import {Alert} from 'react-native';
import Login from '../components/Login';
import firebase from 'react-native-firebase';
import {connect} from  'react-redux';
import {login} from '../../../actions/user';

class LoginScreen extends Component{
    constructor(props){
        super(props);        
    }
    
    loginUser =async  ({email, password}) => {
        try {
            let response = await firebase.auth().signInWithEmailAndPassword(email,password);
            let {user} = response;
            this.props.login(user);
            this.props.navigation.navigate('Loading')
        }catch{
            Alert.alert(
                'Iniciar Sesión', 
                'Email y/o Contraseña incorrectas',
                )
        }}
            
    render(){
    
        return(
            <Login
                setPassword = {this.setPassword}
                setEmail = {this.setEmail}
                loginUser = {this.loginUser}   
                sinContraseña = {() => {this.props.navigation.navigate('Fpw')}}
                resgistropantalla = {() => {this.props.navigation.navigate('Register')}}                        
            />
        );
    }   
}
export default connect (
    (state) => ({user: state.user}),
    {login}
)(LoginScreen);