import React, {Component} from 'react';
import {Alert} from 'react-native';
import Register from '../components/Register';
import firebase from 'react-native-firebase';
import {connect} from  'react-redux';
import {login} from '../../../actions/user';

class RegisterScreen extends Component{
    constructor(props){
        super(props);   
        this.db = firebase.firestore();     
    }

    createUser = async ({name,lastname,cel,email,password,cpassword}) => {
        
        if (password != cpassword)
        {            
            Alert.alert(
                'Contraseña', 
                'Contraseña no coincide'
            )   
        }
        else
        {
            try{
                let response = await firebase.auth().createUserWithEmailAndPassword(email,password);                                     
                let {user} = response; 
                await this.db.collection('Users').doc(user.uid)
                .set({Nombre: name,Apellido: lastname,Email: email,Telefono: cel}); 
                await this.db.collection('afiliadoU').doc(user.uid).set({N:'0', cod: cel});                
                this.props.navigation.navigate('Patrocinador')
            }catch{
                Alert.alert(
                    'Registrar', 
                    'Datos de registro no validos' 
                    ) 
                }              
            }  
        }   

    render(){
        return(
            <Register 
                setPassword = {this.setPassword}
                setEmail = {this.setEmail}
                createUser = {this.createUser}
                setName = {this.setName}
                setLastname = {this.setLastname}
                setCel = {this.setCel}
                setCpassword = {this.setCpassword}
                pantallaLogin = {() => {this.props.navigation.navigate('Login')}}
            />
        );
    }   
}

export default connect (
    (state) => ({user: state.user}),
    {
        login
    }
)(RegisterScreen);