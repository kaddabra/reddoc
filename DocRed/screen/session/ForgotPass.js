import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    ImageBackground,
    TouchableHighlight,
    TouchableOpacity,
    TextInput,
    Alert    
} from 'react-native'
import firebase from 'react-native-firebase';

export default class ForgotPassword extends Component{
    state = {email: ''}

    forgotPassword = () => {
        const{email} = this.state
        firebase
        .auth()
        .sendPasswordResetEmail(email)
        .then(() => Alert.alert(
            'Restablecer Contraseña',
            'Pof favor, revisa tu Email',
            [
                {text: 'Iniciar sesión', onPress: () => this.props.navigation.navigate('Register')}
            ]
            ))
        .catch(() => Alert.alert(
            'Recovery Password', 
            'Invalid Email',
            ))    
    }

    render(){
        return(
            <View style = {{flex:1}}>
                <ImageBackground
                source = {require('../../assets/home.jpg')}
                style = {{flex:1}}> 
                    <View style = {styles.contentAll}>
                    <Text style = {styles.Text}>Forgot Passwod</Text>
                    <TextInput style ={styles.textinputStyle}
                        underlineColorAndroid = "transparent"
                        autoCorrect = {false}
                        placeholder = "Email"
                        placeholderTextColor = "black"
                        autoCapitalize = "none"
                        onChangeText = {email => this.setState({email})}
                        value = {this.state.email}
                    />                    
                    <TouchableOpacity style = {styles.bottonLogin}
                        onPress = {this.forgotPassword}>
                        <Text style = {styles.Text1}>Restore Password</Text>  
                    </TouchableOpacity> 
                    <Text style = {styles.Text2}>Check your emaill</Text>                   
                    <TouchableHighlight onPress={() => this.props.navigation.navigate('Login')}>
                        <Text style = {styles.Text3}>Return</Text>
                    </TouchableHighlight>                                           
                </View>            
            </ImageBackground>
        </View>
        )
    }
}

const styles = StyleSheet.create(
    {
        Text:{
            fontWeight:'bold',
            color: 'black',
            fontSize: 22, 
            marginTop:'20%' 
        },
        Text1:{
            fontWeight:'bold',
            color: 'black',
            fontSize: 18,            
        },
        Text2:{
            color: 'black',
            fontSize: 18,
        },
        Text3:{
            color: '#EE4141',
            fontSize: 18,
            textDecorationLine: 'underline',
            marginTop:'10%' 
        },
        contentAll:{
            alignItems: 'center',
            marginTop: '10%'
        },
        textinputStyle:{
            margin: 8,
            width:'80%', 
            height: 40,
            borderColor: '#B09805',
            borderRadius: 15,
            borderWidth: 1,
            color: 'white',
        },
        bottonLogin: {
            backgroundColor: '#B09805',
            padding: 10,
            margin: 8,
            width:'80%',
            height: 40,
            borderRadius: 15,
            alignItems: 'center'
        }, 
        contentReg:{
            alignItems: 'center',
            flexDirection: 'row',
            marginTop: '10%'
        }
    }
)