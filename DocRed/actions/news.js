export function addNews (news){
    return{
        type: 'ADD_NEWS',
        news: news
    }
}

export function removeNews (news){
    return{
        type: 'REMOVE_NEWS',
        news: news
    }
}