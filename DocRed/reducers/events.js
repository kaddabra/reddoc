export default (state = {}, action) =>{
    switch(action.type){
        case 'ADD_EVENT': 
            return action.event;
        default: 
            return state; 
    }
}