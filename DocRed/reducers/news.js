export default (state = [], action) => {
    switch(action.type){
        case 'ADD_NEWS': 
            return state.concat([action.news]); 
        case 'REMOVE_NEWS':
            return [];    
        default: 
            return state;    
    }    
}