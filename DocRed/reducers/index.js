import {combineReducers} from 'redux';
import user from './user'; 
import event from './events';
import news from './news'

export default combineReducers({
    user,
    event, 
    news   
})  