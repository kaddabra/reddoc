/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './DocRed/screen/session/Routes';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
